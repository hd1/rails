# frozen_string_literal: true
class ArticlesController < ApplicationController
  before_action :load_article, only: :show
  protect_from_forgery with: :null_session # FIXME: allow testing with curl 
  def index
    @featured_article = Article.featured.limit(1).first
    @articles = Article.published.where.not(id: @featured_article).
      paginate(page: params[:page], per_page: 5)
  end

  def show
  end

  def search
    @articles = Article.all
    @articles = @articles.filter { |article|
      if params.has_key?('q')
        article[:score] = (article[:title].scan(params['q']).size)*2 + (article[:body].scan(params['q'])).size
        article[:title].downcase.scan(params['q']) or article[:body].downcase.scan(params['q'])
      end
        article[:score] > 0
    }
    if params.has_key? 'algo' 
      @articles.sort_by!{ |article| -article[:score] }
    end

    render json: @articles 
  end
end

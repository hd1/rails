require 'rails_helper'

Rails.application.load_tasks

describe "import_csv" do
  it "imports data" do
    Rake::Task["import_csv:csv"].invoke
    expect(Article.count == 500)
  end
end


require 'rails_helper'
Rails.application.load_tasks
RSpec.describe "Articles", type: :request do
  describe "GET /articles" do
    it "works! (now write some real specs)" do
      get articles_path
      expect(response).to have_http_status(200)
    end
  end
  
  describe "GET /search" do
    it "returns nil without parameters" do
      get '/search'
      expect(response.parsed_body).to be_truthy
    end

    it "returns article titles and bodies with the string queried" do
      Rake::Task["import_csv:csv"].invoke
      get '/search?q=soluta%20distinctio'
      
      expect(response.parsed_body.length).to eq 1
      response.parsed_body.each do |t|
        expect(t["title"].include? 'soluta distinctio').to be true
      end
    end
    
    it "is sorted by score if algo is given" do
      Rake::Task["import_csv:csv"].invoke
      get '/search?q=quid&algo=1'
      expect(response.parsed_body.length).to eq 307
      response.parsed_body.each_index do |index|
        begin
          expect(response.parsed_body[index]["score"]).to be => response.parsed_body[index+1]["score"]
        rescue NoMethodError
        end
      end
    end
  end
end

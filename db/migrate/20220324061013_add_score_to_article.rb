class AddScoreToArticle < ActiveRecord::Migration[5.0]
  def change
    add_column :articles, :score, :integer, :default => -1
    add_index :articles, :score
  end
end

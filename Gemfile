# frozen_string_literal: true

ruby '2.6.3'

source 'https://rubygems.org'

gem 'coffee-rails', '~> 4.1.0'
gem 'faker'
gem 'friendly_id'
gem 'jbuilder', '~> 2.0'
gem 'jquery-rails'
gem 'jquery-ui-sass-rails'
gem 'pry-byebug'
gem 'pry-nav'
gem 'pygments.rb'
gem 'rails', '5.0.2'
gem 'redcarpet'
gem 'sass-rails', '~> 5.0.4'
gem 'slim-rails', require: ["slim", "slim/smart"]
gem 'sqlite3', '~> 1.3.0'
gem 'uglifier', '>= 1.3.0'
gem 'will_paginate', '~> 3.0.6'

group :development, :test do
  gem 'capybara'
  gem 'factory_bot_rails', '4.10.0'
  gem 'rspec-benchmark'
  gem 'rspec-rails', '~> 3.5'
end

group :test do
  gem 'database_cleaner'
  gem 'rails-controller-testing'
  gem 'selenium-webdriver'
end

group :development do
  gem 'rubocop'
end

group :production do
  gem 'rails_12factor'
  gem 'unicorn'
end

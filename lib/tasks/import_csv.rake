require 'csv'
namespace :import_csv do
    task :csv => :environment do
      # clear tables before importing
      Author.destroy_all
      Article.destroy_all
      csv_text = File.read("db/data/articles.csv")
      csv = CSV.parse(csv_text, :headers => true)
      csv.each do |row|
        author = Author.find_or_create_by(name: row["author name"])
        author.save!
        body = row['body'].sub('[', '').sub(']', '').gsub('"', '')
        article = Article.new({:title =>row["title"], :author => author, :body => body})
        article.save!
      end
      puts "Articles and authors imported"
    end
end
